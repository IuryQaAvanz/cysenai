const { addCompareSnapshotCommand } = require('cypress-visual-regression/dist/command')

addCompareSnapshotCommand({
  capture: 'fullPage',
  fileName: (testName) => {
    return testName.split('describe(')[1].split(')')[0];
  }
});

Cypress.Commands.add('removeElementos', () => {
  cy.get('.main-container-wrapper .sticky-header').invoke('css', 'position', 'relative');
  cy.get('div[aria-label="cookieconsent"]').invoke('remove');
  cy.scrollTo(0,500);

  cy.wait(200);

  cy.get('#adopt-controller-button', { timeout: 10000 }).then(($el) => {
    if ($el.length && $el.is(':visible')) {
      cy.wrap($el).invoke('remove');
    } else {

      cy.log('Elemento #adopt-controller-button não encontrado ou não visível.');
    }
  });

  cy.scrollTo('bottom')
  .then(() => {
    cy.get('.vue-go-top').invoke('remove');
  });

  
  cy.get('.adopt-c-blcsFr').invoke('remove');
  cy.get('a.bubble-chatbot').invoke('remove');
  cy.get('a.whatsapp-chat').invoke('remove');  
  cy.get('#__talkjs_launcher').invoke('remove');
  cy.get('#bricks-component-HjOnT02CEn-K3zPQJilotA-overlay').invoke('remove');
  cy.get('#bricks-component-HjOnT02CEn-K3zPQJilotA-wrapper').invoke('remove');
});

Cypress.Commands.add('limparEstadoCypress', () => {
  cy.clearCookies();
  cy.clearLocalStorage();
  cy.reload(true);
});

Cypress.Commands.add('remove_Elementos', () => {
  cy.get('.main-container-wrapper .sticky-header').invoke('css', 'position', 'relative');
  cy.get('div[aria-label="cookieconsent"]').invoke('remove');
  cy.scrollTo(0, 500);

  cy.scrollTo('bottom').then(() => {
    cy.get('.vue-go-top').invoke('remove');
  });

  cy.get('a.bubble-chatbot').invoke('remove');
  cy.get('a.whatsapp-chat').invoke('remove');
  cy.get('#__talkjs_launcher').invoke('remove');
  cy.get('#bricks-component-HjOnT02CEn-K3zPQJilotA-overlay').invoke('remove');
  cy.get('#bricks-component-HjOnT02CEn-K3zPQJilotA-wrapper').invoke('remove');
});

