const env = require("../../../../fixtures/env.prod.json")
const urls = require("../../../../fixtures/urls.json")

describe(`Suite de testes (DESKTOP) para ${Cypress.env('action')} imagens`, () => {

  beforeEach(() => {
    cy.limparEstadoCypress()  
  })

  it(`Acessa a página HOME e ${Cypress.env('motivoteste')} imagens para validar layout`, () => {
    const homeUrl = `${env.baseUrl}/${urls.HOME}`
    cy.intercept(homeUrl).as('home')
    cy.visit(homeUrl)
    cy.wait(`@home`)

    cy.removeElementos()

    cy.compareSnapshot(`page_HOME`, { 
      blackout: ['#adopt-controller-button'],
      blackout: ['.adopt-c-blcsFr'], 
      capture: 'fullPage',
      errorThreshold: Cypress.env('CYPRESSVR_ERROR_THRESHOLD')  
    })
  })

  it(`Acessa a página CURSOS e ${Cypress.env('motivoteste')} imagens para validar layout`, () => {
    const cursoUrl = `${env.baseUrl}/${urls.CURSOS}`
    cy.intercept(cursoUrl).as('cursos')
    cy.visit(cursoUrl)
    cy.wait(`@cursos`)

    cy.remove_Elementos()

    cy.compareSnapshot(`page_CURSOS`, { 
      blackout: ['#adopt-controller-button'],
      blackout: ['.adopt-c-blcsFr'], 
      capture: 'fullPage',
      errorThreshold: Cypress.env('CYPRESSVR_ERROR_THRESHOLD')  
    })
  })

  for (const [page, url] of Object.entries(urls)) {
    if (page === 'CURSOS' || page === 'HOME') {
      continue;
    }
    it(`Acessa a página ${page} e ${Cypress.env('motivoteste')} imagens para validar layout`, () => {
      const fullUrl = `${env.baseUrl}${url}`
      cy.intercept('GET', fullUrl).as(page)
      cy.visit(fullUrl)
      cy.wait(`@${page}`)

      cy.remove_Elementos()

      cy.compareSnapshot(`page_${page}`, { 
        blackout: ['#adopt-controller-button'],
        blackout: ['.adopt-c-blcsFr'], 
        capture: 'fullPage',
        errorThreshold: Cypress.env('CYPRESSVR_ERROR_THRESHOLD') 
      })
      
    })
  }  
}) 